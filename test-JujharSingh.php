<?php
/**
 * Created by PhpStorm.
 * User: jujha
 * Date: 2018-04-17
 * Time: 11:48 AM
 */


// Question 1

/*
 *
20 kids have gotten together to play baseball, but they need help making
  the teams as even as possible. The kids have all ranked their skill level
  from 1-10. In no particular order, here are their skill numbers:
  8,5,6,9,3,8,2,4,6,10,8,5,6,1,7,10,5,3,7,6.

Write an algorithm (in PHP) that will place these (or any other) kids into
   two teams of equal size with the total skill level being as even as possible.
 */


$data = [8, 5, 6, 9, 3, 8, 2, 4, 6, 10, 8, 5, 6, 1, 7, 10, 5, 3, 7, 6];
$group1 = [];
$group2 = [];

// Order data from high to low
rsort($data);

// get the first five highest values
for ($i = 0; $i > 5; $i++) {
    if ($group1 <= $group2) {
        array_push($group1, $data[$i]);
    } else {
        array_push($group2, $data[$i]);
    }
}

// remove the first five values and reverse the array
//   so there will be even distribution
$data_n = $data;
array_splice($data_n, 0, 5);
sort($data_n);

// Add the last values to the two groups
foreach($data_n as $kid) {
    if ($group1 <= $group2) {
        array_push($group1, $kid);
    } else {
        array_push($group2, $kid);
    }
}

// Output results
print("group1: ");
print_r($group1);
print("");
print("");
print("group2: ");
print_r($group2);

// Outputs
/*

group1: Array
(
    [0] => 1
    [1] => 3
    [2] => 4
    [3] => 5
    [4] => 6
    [5] => 6
    [6] => 7
    [7] => 8
)
group2: Array
(
    [0] => 2
    [1] => 3
    [2] => 5
    [3] => 5
    [4] => 6
    [5] => 6
    [6] => 7
)

*/


// Question 2

/*
 * Draw an image with PHP GD
 */

header("Content-type: image/png");
$width = 350;
$height = 360;
$im = ImageCreateTrueColor($width, $height);
ImageAntiAlias($im, true);

// colours
$yellow = ImageColorAllocate($im, 255, 255, 0);
$blk = ImageColorAllocate($im, 0, 0, 0);
$white = ImageColorAllocate($im, 255, 244, 254);

// background
ImageFillToBorder($im, 0, 0, $yellow, $yellow);

// eyes
ImageEllipse($im, 130, 110, 100, 100, $blk);
ImageEllipse($im, 230, 150, 100, 100, $blk);

// mouth
ImageFilledEllipse($im, 180, 220, 150, 50, $blk);

// teeth
ImageEllipse($im, 160, 200, 15, 20, $white);
ImageEllipse($im, 180, 200, 15, 20, $white);
ImageEllipse($im, 140, 197, 15, 20, $white);


ImagePNG($im);
ImageDestroy($im);


// Question 3
// MySQL query

$query = "SELECT firstName, lastName, address, city FROM Employees
INNER JOIN Addresses ON Employees.employeeID = Addresses.employeeID;";


// Question 4
// OO Question

class item {
    private $name;
    private $weight; // kg
    private $width;  // cm
    private $height;
    private $depth;

    function __construct($name,$weight, $width, $height, $depth) {
        $this->name = $name;
        $this->weight = $weight;
        $this->width = $width;
        $this->height = $height;
        $this->depth = $depth;
    }

    function displayInfo(){
        echo "$this->name: $this->weight cm x $this->height cm x $this->depth cm";
        echo "\n"; // blank line for formatting
    }

    function updateWeight($weight) {
        $this->weight = $weight;
    }

    function updateDimensions($width, $height, $depth) {
        $this->with = $width;
        $this->height = $height;
        $this->depth = $depth;
    }

}

$cat = new item("Cat",1, 60,20,10);
$cat->displayInfo();
$cat->updateDimensions (160,120,110);
$cat->displayInfo();